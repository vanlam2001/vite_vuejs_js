import { createStore } from "vuex";
import { data_shoe } from "./data_shoe";

const store_Ex_Shoe_Shop = createStore({
    state() {
        return {
            listShoe: data_shoe,
            cart: [],
        }
    },
    mutations: {
        ADD_TO_CART(state, payload) {
            const cloneCart = [...state.cart];
            const index = cloneCart.findIndex((item) => {
                return item.id === payload.id;
            });
            if (index === -1) {
                const newShoe = { ...payload, soLuong: 1 };
                cloneCart.push(newShoe);
            } else {
                cloneCart[index].soLuong++;
            }
            state.cart = cloneCart;
        },
        DELETE_SHOE(state, payload) {
            const newCart = state.cart.filter((item) => {
                return item.id !== payload;
            });
            state.cart = newCart;
        },
        CHANGE_QUALITY(state, payload) {
            const cloneCart = [...state.cart];
            const index = cloneCart.findIndex((item) => {
                return item.id === payload.id;
            });
            if (index !== -1) {
                cloneCart[index].soLuong += payload.luaChon;
            }

            state.cart = cloneCart;
        },
    }
});
export default store_Ex_Shoe_Shop;