import { createStore } from "vuex";

const store_Ex_Demo_Vuex_So_Luong = createStore({
    state() {
        return {
            soLuong: 100,
        };
    },
    mutations: {
        tangSoLuong(state) {
            state.soLuong += 2;
        },
        giamSoLuong(state) {
            state.soLuong -= 2;
        }
    },
    actions: {
        tangSoLuong(context) {
            context.commit('tangSoLuong');
        },
        giamSoLuong(context) {
            context.commit('giamSoLuong');
        }
    }
})

export default store_Ex_Demo_Vuex_So_Luong;