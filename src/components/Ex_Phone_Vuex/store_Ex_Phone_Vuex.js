import { createStore } from "vuex";
import { data_phone } from "./data_phone";

const store_Ex_Phone_Vuejs = createStore({
    state() {
        return {
            listPhone: data_phone,
            detailPhone: data_phone[0],
            cart: []
        }
    },
    mutations: {
        XEM_CHI_TIET(state, phone) {
            state.detailPhone = phone;
        },
        THEM_SAN_PHAM(state, payload) {
            const cloneCart = [...state.cart];
            const index = cloneCart.findIndex((item) => {
                return item.maSP === payload.maSP;
            });
            if (index === -1) {
                const newShoe = { ...payload, soLuong: 1 };
                cloneCart.push(newShoe);
            } else {
                cloneCart[index].soLuong++;
            }
            state.cart = cloneCart;
        },
        XOA_SAN_PHAM(state, payload) {
            const newCart = state.cart.filter((item) => {
                return item.maSP === payload.maSP;
            })
            state.cart = newCart;
        },
        CHINH_SO_LUONG(state, payload) {
            const cloneCart = [...state.cart];
            const index = cloneCart.findIndex((item) => {
                return item.maSP === payload.maSP;
            });
            if (index !== -1) {
                cloneCart[index].soLuong += payload.soLuong;
            }
            state.cart = cloneCart;
        }
    }
})

export default store_Ex_Phone_Vuejs;