import { createStore } from "vuex";
import { dataGhe } from "./dataGhe";
import Toastify from 'toastify-js';

const store_Vuex_Dat_Ve = createStore({
    state() {
        return {
            listGhe: dataGhe,
            chonGhe: [],
        }
    },
    mutations: {
        CHON_GHE(state, payload) {
            if (!payload.daDat) {
                let chonGheClone = [...state.chonGhe];
                let dataGheClone = JSON.parse(JSON.stringify(state.listGhe));

                let index = chonGheClone.findIndex((ghe) => {
                    return ghe.soGhe === payload.soGhe;
                });
                if (index === -1) {
                    chonGheClone.push(payload);

                    Toastify({
                        text: `Chọn ghế ${payload.soGhe} thành công`,
                        duration: 3000,
                        style: {
                            background: "linear-gradient(to right, #228B22	, #228B22)",
                        }
                    }).showToast();

                    dataGheClone.forEach(ghe1 => {
                        let index = ghe1.danhSachGhe.findIndex((ghe2) => {
                            return ghe2.soGhe === payload.soGhe;
                        });

                        if (index !== -1) {
                            ghe1.danhSachGhe[index].daDat = "dangDat";
                        }
                    });
                    state.listGhe = dataGheClone;
                    state.chonGhe = chonGheClone;

                }
            }
            else if (payload.daDat === "dangDat") {
                Toastify({
                    text: `Bạn đang chọn ghế ${payload.soGhe}, Vui lòng chọn thêm ghế khác`,
                    duration: 3000,
                    style: {
                        background: "linear-gradient(to right, #FF0000, #FF0000)",
                    },
                }).showToast();
            } else {
                Toastify({
                    text: `Ghế ${payload.soGhe} đã có người chọn. Vui lòng chọn ghế khác`,
                    duration: 3000,
                    style: {
                        background: "linear-gradient(to right, #FF0000, #FF0000)",
                    }
                }).showToast();
            }
        },
        XOA_GHE(state, payload) {
            let chonGheClone = [...state.chonGhe];
            let dataGheClone = [...state.listGhe];

            let locGhe = chonGheClone.filter((ghe) => {
                return ghe.soGhe !== payload.soGhe;
            })

            dataGheClone.forEach(ghe1 => {
                let index = ghe1.danhSachGhe.findIndex((ghe2) => {
                    return ghe2.soGhe === payload.soGhe;
                })

                if (index !== -1) {
                    ghe1.danhSachGhe[index].daDat = false;
                }

            });
            state.listGhe = dataGheClone;
            state.chonGhe = locGhe;
        }
    }
})

export default store_Vuex_Dat_Ve;

