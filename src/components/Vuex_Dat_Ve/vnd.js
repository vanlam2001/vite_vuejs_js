const vnd = new Intl.NumberFormat('vin-VN', {
    style: 'currency',
    currency: 'VND',
})

export default vnd;