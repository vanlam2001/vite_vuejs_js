import { createApp } from 'vue'
import './style.css'

import App from './App.vue'
import store_Demo_Redux_Mini from './components/Ex_Demo_Vuex_Mini/store_Demo_Redux_Mini';
import store_Ex_Demo_Vuex_So_Luong from './components/Ex_Demo_Vuex_So_Luong/store_Ex_Demo_Vuex_So_Luong';
import store_Ex_Shoe_Shop from './components/Ex_Shoe_Shop_Vuex/store_Ex_Shoe_Shop';
import store_Ex_Phone_Vuejs from './components/Ex_Phone_Vuex/store_Ex_Phone_Vuex';
import store_Vuex_Dat_Ve from './components/Vuex_Dat_Ve/store_Vuex_Dat_Ve';
import FoodItems from './components/Ex_Router/FoodItems.vue'
import AnimalCollection from './components/Ex_Router/AnimalCollection.vue'


const app = createApp(App);

app.use(store_Demo_Redux_Mini); // Sử dụng Vuex store
app.use(store_Ex_Demo_Vuex_So_Luong);
app.use(store_Ex_Shoe_Shop);
app.use(store_Ex_Phone_Vuejs);
app.use(store_Vuex_Dat_Ve);
app.component('food-items', FoodItems);
app.component('animal-collection', AnimalCollection)

app.mount('#app');







